using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<T> ();

            if ((object)_instance == null)
                Debug.LogError ((object)string.Format ("Singleton: No {0} instance set", (object)typeof (T).FullName));

            return Singleton<T>._instance;
        }
    }

    public static bool IsInstantiated
    {
        get
        {
            return (object)Singleton<T>._instance != null;
        }
    }

    private void Awake ()
    {
        if (!Instance) ;
    }

    private void OnDestroy ()
    {
        _instance = null;
    }
}